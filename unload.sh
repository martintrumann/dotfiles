#!/bin/bash
dir=`dirname "$0"`
cp -r $dir/files/. ~/

if groups | grep -Eq 'sudo|wheel'; then
	if [[ ! -d /usr/local/share/kbd/keymaps ]]; then
		sudo mkdir -p /usr/local/share/kbd/keymaps/
		sudo cp $dir/extra/custom-uk.map /usr/local/share/kbd/keymaps/custom-uk.map
		sudo cp $dir/extra/vconsole.conf /etc/vconsole.conf
	fi

	if [[ ! -e /etc/systemd/system/getty@tty1.service.d/skip-username.conf ]]; then
		sudo cp $dir/extra/skip-username.conf /etc/systemd/system/getty@tty1.service.d/skip-username.conf
	fi
fi

if [[ ! -d ~/.config/nvim/ ]]; then
	git clone https://gitlab.com:martintrumann/dotnvim.git ~/.config/nvim/
fi
