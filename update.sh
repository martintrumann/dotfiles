#!/bin/bash

for line in $(cat ./tracked.txt); do
	if [[ ! -e ~/$line ]]; then
		echo not found: ~/$line
		continue
	fi

	dir=$(dirname $line)
	if [[ ! -d ./files/$dir ]]; then
		mkdir ./files/$dir
	fi

	cp -rT ~/$line ./files/$line
done
