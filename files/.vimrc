" Options for Vim
set laststatus=2
set statusline=%F\ %m\ %=\ %l,%c%V

syntax on
colorscheme default
filetype plugin on

let g:python_recommended_style = 0

set number
set exrc
set secure
set autoindent
set copyindent
set showmode
set hlsearch
set ruler
set relativenumber

set autowrite
set autoread

set showcmd

set mouse=a

set path+=**
set wildmenu
set tabstop=4
set softtabstop=4
set shiftwidth=4

set ignorecase
set smartcase

set foldmethod=marker
set foldlevel=99
set foldmarker={,}

set list listchars=tab:→\ ,space:·,nbsp:␣,trail:•,eol:¶,precedes:«,extends:»

set encoding=utf-8

set matchpairs+=<:>

let mapleader = " "
let g:mapleader = " "

vnoremap <tab> >

nnoremap <leader><leader> :

nnoremap <leader>/ 0i//<esc>

nnoremap <leader>w :w<cr>
nnoremap <leader>aw :wa<cr>
nnoremap <leader>q :q<cr>
nnoremap <leader>aq :qa<cr>
nnoremap <leader>t :tabnew<cr>

nnoremap <leader>m :make -s!<cr>
nnoremap <leader>c :clist<cr>

nnoremap <leader>n :noh<CR>
nnoremap <leader>e :e 
nnoremap <leader>o :e 
nnoremap <leader>s :vsplit 

nnoremap <leader>p :set paste<CR>"+p:set nopaste<CR>
vnoremap <leader>p <Esc>:set paste<CR>gv"+p:set nopaste<CR>

vnoremap <leader>y "+y

vnoremap // y/\V<C-r>=escape(@",'/\')<CR><CR>

nnoremap <leader>h gT
nnoremap <leader>l gt

noremap h j
noremap t k

noremap k l
noremap j h

noremap l t

nnoremap <C-h> 20j
nnoremap <C-t> 20k

vnoremap  <esc>
inoremap  <esc>

nnoremap <C-w>d <C-W>h
nnoremap <C-w>h <C-W>j
nnoremap <C-w>t <C-W>k
nnoremap <C-w>n <C-W>l

nnoremap <C-w><S-d> <C-W><S-h>
nnoremap <C-w><S-h> <C-W><S-j>
nnoremap <C-w><S-t> <C-W><S-k>
nnoremap <C-w><S-n> <C-W><S-l>

" netrw {1
let g:netrw_banner=0
let g:netrw_altv=1
"1}

let g:bash_Ctrl_j = 'off'
let g:bash_Ctrl_h = 'off'
let g:bash_Ctrl_l = 'off'
let g:bash_Ctrl_u = 'off'
let g:bash_Ctrl_a = 'off'

vnoremap # *
vnoremap * #

nnoremap <leader>; A;<ESC>

nnoremap <C-i> gt

command! TabIt %s/\ \ \ \ /	/g

command! MakeTags !ctags -R .

augroup project
	autocmd!
	autocmd BufRead,BufNewFile *.h,*.c set filetype=c.doxygen
augroup END

augroup expandZip
	autocmd!
	autocmd BufReadCmd *.od* call zip#Browse(expand("<amatch>"))
augroup END
