config.load_autoconfig(False)

c.qt.args = ["shm", "disable-gpu"]

c.colors.webpage.preferred_color_scheme = "dark"
c.colors.webpage.bg = "#333"

c.colors.tabs.even.bg = "black"
c.colors.tabs.odd.bg = "black"
c.colors.tabs.selected.even.bg = "grey"
c.colors.tabs.selected.odd.bg = "grey"

c.content.pdfjs = True

c.fonts.default_family = "terminus"
c.fonts.default_size = "12pt"

c.fonts.tabs.selected = "12pt default_family"
c.fonts.tabs.unselected = "12pt default_family"

c.tabs.last_close = "ignore"
c.tabs.title.format = "{index}: {current_title} {audio}"
c.tabs.show = "multiple"
c.tabs.show_switching_delay = 5000

c.confirm_quit = ["always"]

c.auto_save.session = True

c.url.searchengines = {"DEFAULT": "https://duckduckgo.com/?q={}"}
c.url.searchengines["d"] = "https://duckduckgo.com/?q={}"
c.url.searchengines["arch"] = "https://wiki.archlinux.org/?search={}"
c.url.searchengines["a"] = "https://wiki.archlinux.org/?search={}"
c.url.searchengines["ph"] = "https://www.php.net/manual-lookup.php?scope=quickref&pattern={}"
c.url.searchengines["r"] = "https://www.reddit.com/r/{}"
c.url.searchengines["rs"] = "file:///home/martin/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/share/doc/rust/html/std/index.html?search={}"
c.url.searchengines["mdn"] = "https://developer.mozilla.org/en-US/search?q={}"
c.url.searchengines["maria"] = "https://mariadb.com/kb/en/+search/?q={}"

c.completion.open_categories = ["quickmarks", "bookmarks", "history", "searchengines"]

c.hints.chars = "aoeuidhtns"

def bind(modes, key, cmd):
    for m in modes:
        match m:
            case "i":
                mode = "insert"
            case "n":
                mode = "normal"
            case _:
                return
        config.bind(key, cmd, mode)

bind("i", "<Ctrl-w>", "fake-key <Ctrl-Backspace>")

config.bind('<space>t', 'open -t')
config.bind('<space>q', 'tab-close')
config.bind('<space>d', 'config-source')

config.bind('<Ctrl-n>', 'search-prev')

config.unbind('l')
config.bind('ld', 'back -t')
config.bind('ln', 'forward -t')

config.bind('h', 'scroll down')
config.bind('t', 'scroll up')

config.bind('D', 'back')
config.bind('H', 'tab-prev')
config.bind('T', 'tab-next')
config.bind('N', 'forward')

config.bind('j', 'scroll-page 0 0.5')
config.bind('k', 'scroll-page 0 -0.5')

config.bind(';p', 'hint all run :open -p {hint-url}')

config.bind('e', 'hint all')
config.bind('E', 'hint all tab-fg')

config.bind('<Ctrl-q>', "close")
config.bind('<Ctrl-r>', "greasemonkey-reload ;; reload")
