-- vim: ts=4 sw=4 noet ai cindent syntax=lua

conky.config = {
    out_to_x = false,
    background = false,
    border_width = 1,
    cpu_avg_samples = 2,
    default_color = 'white',
    default_outline_color = 'white',
    default_shade_color = 'white',
    draw_borders = true,
    draw_graph_borders = true,
    draw_outline = false,
    draw_shades = false,
    extra_newline = false,
    net_avg_samples = 2,
    out_to_console = true,
    out_to_ncurses = true,
    out_to_stderr = false,
    show_graph_range = false,
    show_graph_scale = false,
    stippled_borders = 0,
    update_interval = 1.0,
    uppercase = false,
    use_spacer = 'none',
    format_human_readable = true,
}

conky.text = [[
${color green}=System===========================${color}
Uptime ${goto 15} $uptime
Battery ${goto 15} ${battery_percent BAT0}%
Temp ${goto 15} ${hwmon temp 1}°C
Processes ${goto 15} $processes ($running_processes running)
Load ${goto 15} $loadavg

${color green}=CPU=and=Memory===================${color}
${color yellow}CPU 1${color} ${cpu cpu1}%
${cpubar cpu1 10,34}

${color yellow}CPU 2${color} ${cpu cpu2}%
${cpubar cpu2 10,34}


${color yellow}Ram${color} $memperc%  $mem / $memmax
${membar 10,34}

${color yellow}Swap${color} $swapperc%  $swap / $swapmax
${swapbar 10,34}

${color green}=Processes========================${color}
${color yellow}Highest CPU ${goto 22} CPU%   MEM%${color}
${top name 1} ${goto 20} ${top cpu 1} ${top mem 1}
${top name 2} ${goto 20} ${top cpu 2} ${top mem 2}
${top name 3} ${goto 20} ${top cpu 3} ${top mem 3}
${top name 4} ${goto 20} ${top cpu 4} ${top mem 4}
${top name 5} ${goto 20} ${top cpu 5} ${top mem 5}
${top name 6} ${goto 20} ${top cpu 6} ${top mem 6}

${color yellow}Highest MEM ${goto 22} CPU%   MEM%${color}
${top_mem name 1} ${goto 20} ${top_mem cpu 1} ${top_mem mem 1}
${top_mem name 2} ${goto 20} ${top_mem cpu 2} ${top_mem mem 2}
${top_mem name 3} ${goto 20} ${top_mem cpu 3} ${top_mem mem 3}
${top_mem name 4} ${goto 20} ${top_mem cpu 4} ${top_mem mem 4}
${top_mem name 5} ${goto 20} ${top_mem cpu 5} ${top_mem mem 5}
${top_mem name 6} ${goto 20} ${top_mem cpu 6} ${top_mem mem 6}

${color green}=Filesystem=======================${color}
${color yellow}/: ${color}
${fs_free /} / ${fs_size /}
${fs_bar 10,34 /}

${color green}=Network==========================${color}
${color yellow}wlp3s0: ${color}
${wireless_link_bar 10,34 wlp3s0}
Down ${downspeed wlp3s0} ${goto 20} Up ${upspeed wlp3s0}
Total ${totaldown wlp3s0} ${goto 20} Total ${totalup wlp3s0}${font}

${color yellow}eno1: ${color}
Down ${downspeed eno1} ${goto 20} Up ${upspeed eno1}
Total ${totaldown eno1} ${goto 20} Total ${totalup eno1}${font}
]]
