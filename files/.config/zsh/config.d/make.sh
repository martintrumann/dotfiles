# make and other compilation related functions
function m()  {make $@}
function mr() {
	if [[ -e ./run ]]; then
		echo "./run $@"
		./run $@
	else
		make run $@
	fi
}
function mc() {make check $@}
function mt() {make test $@}

function mp() {make publish $@}

function cg() {cargo $@}
function cr() {cargo run $@}
function ch() {cargo check $@}
function ct() {cargo test $@}
function ctnc() {cargo test -- --nocapture $@}

function wp() {wasm-pack $@}
function wb() {wasm-pack build --dev --features dev $@}

function cn() {
	local cmd=("cargo-component" "new" "--editor" "none" "--lib")

	echo $cmd --namespace $1 $2
}

function cb() {
	local cmd=("cargo-component" "build")
	if [[ -e "wasm" ]]; then 
		cmd+=("--target" "wasm32-unknown-unknown")
	fi
	if [[ $1 == "release" ]]; then
		cmd+="--release"
	fi
	$cmd
}

function cc() {
	if [[ $1 != "" && $1 != "release" ]]; then
		cargo-component $@
		return
	fi

	cb $@
}

