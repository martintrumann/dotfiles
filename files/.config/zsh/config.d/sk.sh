# Making commands use sk
if command -v sk &> /dev/null && command -v fd &> /dev/null; then
	# Finding files
	skfiles(){
		if command -v rg &> /dev/null; then
			local cmd="rg --files"
		else
			local cmd="fd --type=f"
		fi

		if [[ -d $1 ]]; then
			echo $(sk -c "$cmd {}" --cmd-query="${1}" -q ${2:-""})
		else
			echo $(sk -c "$cmd {}" -q ${1:-""})
		fi
	}

	skhfiles(){
		if command -v rg &> /dev/null; then
			local cmd="rg --files --hidden"
		else
			local cmd="fd --type=f -H .}"
		fi

		if [[ -d $1 ]]; then
			echo $(sk -c "$cmd {}" --cmd-query="${1}" -q ${2:-""})
		else
			echo $(sk -c "$cmd {}" -q ${1:-""})
		fi
	}

	ask(){
		if [[ -d $1 ]]; then
			echo $(sk -c "fd . {}" --cmd-query="${1}" -q ${2:-""})
		else
			echo $(sk -c "fd . {}" -q ${1:-""})
		fi
	}

	# cd
	cdsk(){
		if [[ -d $1 ]]; then
			local pth=$(sk -c "fd --type=d . {}" --cmd-query="${1}" -q ${2:-""})
		else
			local pth=$(sk -c "fd --type=d . {}" -q ${1:-""})
		fi

		if [[ $pth != "" ]]; then
			cd $pth
		fi
	}

	#listing files
	lsk(){
		if [[ -d $1 ]]; then
			local pth=$(sk -c "fd -i . {}" --cmd-query="${1}" -q ${2:-""})
		else
			local pth=$(sk -c "fd -i {} ." -q ${1:-""})
		fi

		if [[ $pth != "" ]]; then
			l $pth
		fi
	}

	lhsk(){
		if [[ -d $1 ]]; then
			local pth=$(sk -c "fd -Hi . {}" --cmd-query="${1}" -q ${2:-""})
		else
			local pth=$(sk -c "fd -Hi . {}" -q ${1:-""})
		fi

		if [[ $pth != "" ]]; then
			l $pth
		fi
	}

	# Editing files
	ehsk(){
		local pth=$(skhfiles $1 $2)

		if [[ $pth != "" ]]; then
			e $pth
		fi
	}

	sesk(){
		local pth=$(skfiles $1 $2)

		if [[ $pth != "" ]]; then
			se $pth
		fi
	}

	esk(){
		local pth=$(skfiles $1 $2)

		if [[ $pth != "" ]]; then
			e $pth
		fi
	}
fi
