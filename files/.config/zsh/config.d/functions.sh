function mksh() {
	if [[ -e $1 ]]; then
		echo "Error: file exists"
		return
	fi

	echo "" > $1
	chmod u+x $1
	e $1
}

function ytm()  {
	if [[ $1 == "" ]]; then
		if [[ $WAYLAND_DISPLAY != "" ]]; then
			local str=$(wl-paste -p)
		else
			local str=$(xclip -o -selection primary)
		fi

		if [[ $str == "" ]]; then
			echo "No url given"
		else
			yt-dlp -x --audio-format opus --add-metadata $str
		fi
	else
		yt-dlp -x --audio-format opus --add-metadata $@
	fi
}

function crep()  {
	type $1
	while true
		do $@ $(read -E)
	done
}


function mkcd() {
	if [[ -e $1 || $1 == ".." ]]; then
		cd $1
	elif [[ $1 != "" ]]; then
		mkdir -p $1 && cd $1
	fi
}
