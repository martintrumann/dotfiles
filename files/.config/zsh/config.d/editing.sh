# config related to file editing and nvim

if [[ $EDITOR == "nvim" ]]; then
	alias v="nvim -u ~/.config/nvim/pager.lua"
	alias nome='nvim -M'
fi
alias h="helix"

alias e="$EDITOR"
alias se="sudo nvim"
