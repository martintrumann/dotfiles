# ls related configuration for zsh
if command -v exa &> /dev/null; then
	(( ${+aliases[ls]} )) && unalias ls
	function ls() {exa --group-directories-first $@}

	alias tree="ls -T -L 3"

	alias lg="ls -l --git"
fi

alias sl="ls"

alias la='ls -a'

alias ll='ls -l'
alias lla='ls -la'
(( ${+aliases[l]} )) && unalias l
