## Systemd aliases
# global aliases
function sc() {systemctl $@}
function scs() {systemctl start $@}

# user aliases
function scu() {systemctl --user $@}
function scus() {systemctl --user start $@}

function journal() {journalctl -b $@}
