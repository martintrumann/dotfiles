if command -v rustc &> /dev/null; then
	export RUST_SRC_PATH=$(rustc --print sysroot)/lib/rustlib/src/rust/library/
fi

if [[ -d $HOME/disks/falib/node ]]; then
	export npm_config_prefix="$HOME/disks/falib/node/"
	path+=("$HOME/disks/falib/node/bin")
fi

if [[ -e $ZDOTDIR/.zshenv.local ]]; then
	source $ZDOTDIR/.zshenv.local
fi
