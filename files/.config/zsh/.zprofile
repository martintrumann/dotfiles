# If running from tty1 start sway
local use_sway=0

if [[ -z "$DISPLAY" && "$(tty)" == "/dev/tty1" ]]; then
	if [[ $use_sway == 1 ]]; then
		source ~/.config/sway/env
		exec systemd-cat -t sway sway --unsupported-gpu
	else
		exec startx
	fi
fi
