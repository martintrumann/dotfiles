# Minimal i3 config
set $mod Mod4

exec bash ~/.fehbg
exec setxkbmap -layout ee\(dvorak\),gb -option ctrl:nocaps -option grp:alt_space_toggle

hide_edge_borders smart
floating_modifier $mod
focus_follows_mouse no

bindsym $mod+1 workspace 1
bindsym $mod+2 workspace 2
bindsym $mod+3 workspace 3

bindsym $mod+Shift+1 move container to workspace 1
bindsym $mod+Shift+2 move container to workspace 2
bindsym $mod+Shift+3 move container to workspace 3

# fake-outputs 4480x1080+0+0

bindsym $mod+h focus left
bindsym $mod+j focus left
bindsym $mod+l focus right
bindsym $mod+t focus right

bindsym $mod+Shift+h move left
bindsym $mod+Shift+l move right

bindsym $mod+f fullscreen

bindsym $mod+Shift+space floating toggle

bindsym $mod+Shift+q kill

set $run "rofi -modi window,drun,run,ssh -show drun"
bindsym $mod+d exec $run
bindsym $mod+e exec $run

bindsym $mod+a layout tabbed

bindsym $mod+Return exec alacritty

bindsym XF86AudioRaiseVolume exec pactl set-sink-volume @DEFAULT_SINK@ +1%
bindsym XF86AudioLowerVolume exec pactl set-sink-volume @DEFAULT_SINK@ -1%
bindsym XF86AudioMute exec npactl set-sink-mute @DEFAULT_SINK@ toggle

bindsym XF86AudioPlay exec playerctl play-pause || mpc toggle
bindsym Pause         exec playerctl play-pause || mpc toggle
bindsym XF86AudioNext exec playerctl next || mpc seek 100%
bindsym $mod+Prior    exec playerctl next || mpc seek 100%
bindsym XF86AudioPrev exec playerctl prev || mpc prev
bindsym $mod+Next     exec playerctl prev || mpc prev
bindsym XF86AudioStop exec playerctl stop

mode "(l)ock, (e)xit, (s)uspend, (r)eboot, (Shift+s)hutdown" {
	bindsym l exec --no-startup-id $lock, mode "default"
	bindsym s exec --no-startup-id systemctl suspend, mode "default"
	bindsym e exec --no-startup-id i3-msg exit , mode "default"
	bindsym r exec --no-startup-id reboot, mode "default"
	bindsym Shift+s exec --no-startup-id shutdown now, mode "default"
	# exit system mode: "Enter" or "Escape"
	bindsym Return mode "default"
	bindsym Escape mode "default"
}
bindsym $mod+Shift+s mode "(l)ock, (e)xit, (s)uspend, (r)eboot, (Shift+s)hutdown"

bar {
	position top
	# font pango:DejaVu Sans Mono 12px
	font pango:terminus 10px
	tray_output primary
	strip_workspace_numbers on

	status_command i3status
}
