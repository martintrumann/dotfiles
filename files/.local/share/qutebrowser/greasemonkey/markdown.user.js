// ==UserScript==
// @name          markdown
// @namespace     http://localhost
// @description   render markdown
// @include       *.md
// ==/UserScript==

(function () {
	let body = document.getElementsByTagName("body")[0];
	if (!body || body.children.length > 1) return;

	let inElm = document.querySelector("body > pre");
	if (!inElm) return;

	let outElm = document.createElement("div");
	outElm.classList.add("mdOut");
	body.appendChild(outElm);

	let script = document.createElement("script");
	script.type = "text/javascript";
	script.src = "https://cdn.jsdelivr.net/npm/marked/marked.min.js";
	body.appendChild(script);

	setTimeout(() => {
		inElm.style.display = "none";
		let text = inElm.innerText;

		// Don't show Zola header
		if (text.startsWith("+++")) text = text.split("+++")[2];

		outElm.innerHTML = marked.parse(text);
	}, 500);

	document.head.insertAdjacentHTML(
		"beforeEnd",
		`
<style>
body {
	margin: auto;
	max-width: 1000px;
}

h3 {
	font-size: 1rem;
}

.mdOut {
	font-size: 14px;
}

</style>

<style media="screen and (prefers-color-scheme:light)">

body {
	background: white;
}

</style>

<style media="screen and (prefers-color-scheme:dark)">

html {
	background-color: #333;
	color: white;
}

code {
	color: hsla(344, 69%, 60%, 1);
}

a {
	color: #609fff;
}

a:visited {
	color: #968eff;
}

a:active {
	color: #f00;
}

</style>
`
	);
})();
