// ==UserScript==
// @name          Dark mode
// @namespace     http://localhost
// @description   dark mode
// ==/UserScript==

let textcolor = "#eee";

let whiteback = [
	"grafana.com/docs/grafana",
	"https://benchmarksgame-team.pages.debian.net",
	"alpinejs.dev/",
	"paolini.net",
	"tera.netlify.app",
	"klick.ee",
	"tootukassa.ee",
	"inertiajs.com",
	"amazon.com",
	"maketecheasier.com",
	"chartjs.org/docs/next/",
	"wayland-book.com",
	"web.dev",
	"ois2.ttu.ee",
];

let rules = {
	"laravel.com": {
		body: "{background-color: #333; }",
		".docs_main": {
			p: "{color: " + textcolor + ";}",
			h1: "{color: " + textcolor + ";}",
			"h2 a": "{color: " + textcolor + ";}",
			"h3 a": "{color: " + textcolor + ";}",
			h4: "{color: " + textcolor + ";}",
			"h4 a": "{color: " + textcolor + ";}",
			li: "{color: " + textcolor + " !important;}",
			":not(pre) code": "{background: #ddd;}",
			table: "{color: " + textcolor + " !important;}",
			"h1+ul li a": "{color: " + textcolor + "}",
		},
		"pre[class*=language-]": "{ background: #ddd;}",
		"pre[class*=language-] ::selection": "{ background: #fff !important;}",
		"code::selection, code ::selection": "{ background: #fff !important;}",
		".token.comment": "{color: #5a5a5a;}",
		".shadow-lg": "{background-color: #555; }",
	},
	"drewdevault.com": {
		body: "{background: #333; color: " + textcolor + "}",
		"a, a:hover, a code": "{color: hsla(208, 56%, 65%, 1)}",
		h1: "{color: #eee !important}",
		h2: "{color: #eee !important}",
		code: "{color: hsla(344, 69%, 60%, 1)}",
		".webring .article": "{background-color: #666}",
	},
	"gnu.org": {
		body: "{background: #333; color: " + textcolor + "}",
		"a, a:hover, a code": "{color: hsla(208, 56%, 65%, 1)}",
		h1: "{color: #eee !important}",
		h2: "{color: #eee !important}",
		code: "{color: hsla(344, 69%, 60%, 1)}",
		".yyterm": "{color: black;}",
	},
	"sqlite.org": {
		body: "{background: #333; color: " + textcolor + "}",
		"a, a:hover, a code": "{color: hsla(208, 56%, 65%, 1)}",
		h1: "{color: #eee !important}",
		h2: "{color: #eee !important}",
		code: "{color: hsla(344, 69%, 60%, 1)}",
		".yyterm": "{color: black;}",
	},
	"qute://": {
		body: "{background: #333; color: " + textcolor + "}",
		"ul > li > *": "{color: " + textcolor + "}",
		code: "{color: hsla(344, 75%, 60%, 1)}",
		strong: "{color: #6d98ff}",
		em: "{color: #8f8fff}",
		".section-header > a > code": "{color: hsla(344, 69%, 60%, 1)}",
		"a, a:hover, a code": "{color: hsla(208, 56%, 65%, 1)}",
		h1: "{color: #eee !important}",
		h2: "{color: #eee !important}",
		h3: "{color: #eee !important}",
		h4: "{color: #eee !important}",
		"tbody tr:nth-child(odd)": "{ background-color: #222 !important }",
	},
	"rust/html/index.html": {
		body: "{background: #333; color: " + textcolor + "}",
		code: "{color: hsla(344, 69%, 60%, 1)}",
		".section-header > a > code": "{color: hsla(344, 69%, 60%, 1)}",
		"a, a:hover, a code": "{color: hsla(208, 56%, 65%, 1)}",
		"h1 a, h1": "{color: #eee !important}",
		"h2 a": "{color: #eee !important}",
		"h3 a": "{color: #eee !important}",
		"h4 a": "{color: #eee !important}",
	},
	"r/.*.txt/": {
		body: "{background: #333;}",
		pre: "{ color: " + textcolor + "; font-size: 1.2rem; }",
	},
};

function escapeRegex(str) {
	return str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
}

function f(rs, ss) {
	let o = "";
	for (let s in rs) {
		let r = rs[s];
		let fs;
		if (ss !== undefined) {
			if (s == "") {
				fs = ss;
			} else {
				fs = ss + " " + s;
			}
		} else {
			fs = s;
		}
		if (Array.isArray(r)) {
			o += fs + " {" + r.join("; ") + ";}\n";
		} else {
			if (typeof r === "object") {
				o += f(r, fs);
			} else {
				o += fs + " ";
				if (r[0] == "{") {
					o += r + "\n";
				} else {
					r = "{" + r + ";}\n";
					o += r;
				}
			}
		}
	}
	return o;
}

for (let site of whiteback) {
	if (new RegExp(escapeRegex(site)).test(window.location.href)) {
		document.head.innerHTML += `<style>body {background: white}</style>`;
	}
}

for (let rule in rules) {
	let regex;
	if (rule.startsWith("r/")) {
		regex = new RegExp(rule.slice(2, -1));
	} else {
		regex = new RegExp(escapeRegex(rule));
	}

	if (regex.test(window.location.href)) {
		let css = rules[rule];

		console.log(css);

		document.head.innerHTML += "<style>" + f(css) + "</style>";
	}
}

if (typeof window.addRow === "function") {
	let style =
		"#nameColumnHeader, #nameColumnHeader, .detailsColumn, body {background-color: #333; color: #ccc !important} td a, #parentDirText { color: #968eff !important; }";

	document.head.innerHTML += `<style>${style}</style>`;
}
