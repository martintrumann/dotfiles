#!/bin/bash

state=$(xinput list-props "ETPS/2 Elantech Touchpad" | grep Device\ Enabled | sed -e "s/.*\([0-9]\)/\1/")

if [ $state -eq 1 ] 
then
	notify-send 'Touchpad On' --icon=input-mouse
else 
	notify-send 'Touchpad Off' --icon=input-mouse
fi
