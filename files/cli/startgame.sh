#!/bin/zsh
res=$(rg -i "name.*$1" ~/disks/falib/SteamLibrary/steamapps/appmanifest_* /home/martin/.local/share/Steam/steamapps/appmanifest_*)

matches=$(echo $res | rg -e "\d+.+" -o | rg -r "" ".acf|\"name\"|\t" | rg -r " " "\"")

id=$(echo $matches | sk -10 | rg -oe "\d+")

if [[ $id != "" ]] then
	steam steam://run/$id
fi
