alias q="exit"

alias yt="youtube-dl --audio-format vorbis -x"
# Package managers {2
alias aptinst="sudo apt install"
alias aptupg="sudo apt update && sudo apt dist-upgrade"

alias Syu="sudo pacman -Syu"
alias S="sudo pacman -S"

# files {2
alias f="ranger"

alias df="df -h"

if [[ -x /bin/exa ]]; then
	alias ls='exa --group-directories-first'

	alias tree="ls -T -L 3"

	alias lg="ls -l --git"
fi

alias sl="ls"

alias la='ls -a'

alias ll='ls -l'
alias lla='ls -la'

alias pso="ps -H -ouser,pid,pcpu,pmem,stat,args"

# network {2
alias nmrw='nmcli r wifi '
alias nmdw='nmcli d w '

# Pager
alias less="less -Fx4"

# editor {2
EDITOR="nvim"
alias e="$EDITOR"
alias se="sudo $EDITOR"

alias nome='e -M'

# misc {2
alias c="clear"

# Systemd
alias ssys="sudo systemctl"
alias sc="systemctl"
alias scs="systemctl start"
alias scu="systemctl --user"
alias scus="systemctl --user start"

alias k9="killall -9"

alias py="python3"

alias mr="make run"
alias m="make"

alias cconky="conky -c .config/conky/console.conf"
alias cdc="cd && c"

if command -v sk &> /dev/null; then
	if command -v fd &> /dev/null; then
		alias cdsk="cd \$(fd --type=d | sk)"

		if command -v rg &> /dev/null; then
			alias lsk="l \$(rg --files | sk)"
			alias esk="e \$(rg --files | sk)"
		else
			alias lsk="l \$(fd --type=f | sk)"
			alias esk="e \$(fd --type=f | sk)"
		fi
	elif command -v rg &> /dev/null; then
		alias cdsk="cd \$(sk)"
		alias lsk="l \$(rg --files | sk)"
		alias esk="e \$(rg --files | sk)"
	else
		alias cdsk="cd \$(sk)"
		alias lsk="l \$(sk)"
		alias esk="e \$(sk)"
	fi
fi
