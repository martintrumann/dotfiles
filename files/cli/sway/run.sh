#!/bin/bash
if [[ "$1" == "mobile" ]]; then
	exec systemd-cat -t sway sway -c ~/.config/sway/mobile
else
	source ~/.config/sway/env
	exec systemd-cat -t sway sway --unsupported-gpu
fi
