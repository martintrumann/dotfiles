#!/bin/bash
swaymsg input type:touchpad events toggle enabled disabled

state=$(swaymsg -t get_inputs | grep -A 3 touchpad | grep events | sed -e 's/.*\"\(.*\)\".*/\1/')
notify-send -a Touchpad -t 1000 "Touchpad $state" --icon=input-mouse

