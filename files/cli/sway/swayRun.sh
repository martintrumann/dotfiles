#!/bin/bash
export QT_QPA_PLATFORM=wayland
export XDG_SESSION_TYPE=wayland
export BEMENU_BACKEND=wayland
export MOZ_ENABLE_WAYLAND=1

export XDG_CURRENT_DESKTOP=Unity

export WLR_NO_HARDWARE_CURSORS=1

#xdg-settings set default-web-browser org.qutebrowser.qutebrowser.desktop
#xdg-settings set default-web-browser brave-wayland.desktop
#export BROWSER=qutebrowser
#export TERMINAL=alacritty

if [[ $1 == "mobile"]]; then
	exec systemd-cat -t sway sway -c ~/.config/sway/mobile
else
	exec systemd-cat -t sway sway --my-next-gpu-wont-be-nvidia
fi

