#!/bin/bash
color=$(grim -g "$(slurp -p)" - -t png -o | convert png:- -format '%[pixel:s]\n' info:- | sed "s/s//")

notify-send -a "Color Picker" "$color"
