#!/bin/zsh

local pic=/usr/share/icons/HighContrast/48x48/status/alarm.png
local name=`basename $0`

if [[ $name == "alarm" ]]; then
	systemd-run --user --on-calendar=$1 notify-send -i $pic $argv[2,-1]
elif [[ $name == "timer" ]]; then
	systemd-run --user --on-active=$1 notify-send -i $pic $argv[2,-1]
elif [[ $1 == "install" ]]; then
	ln -sr $0 ~/.local/bin/alarm
	ln -sr $0 ~/.local/bin/timer
fi
