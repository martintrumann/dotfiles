#!/bin/bash

touchdev="ETPS/2\ Elantech\ Touchpad"

state=$(xinput list-props "$touchdev" | grep Device\ Enabled | sed -e "s/.*\([0-9]\)/\1/")

if [ $state -eq 1 ]
then
	notify-send 'Touchpad Off' --icon=input-mouse
	xinput disable "$touchdev"

else
	notify-send 'Touchpad on' --icon=input-mouse
	xinput enable "$touchdev"
fi
