#!/bin/bash

find -name "*.tex" | entr -r latexmk -pdf -halt-on-error $1
