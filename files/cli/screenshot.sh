#!/bin/bash

if [[ $1 == "select" ]]; then
	selection=$(hacksaw -f "-i %i -g %g")
	if [[ $2 == "file" ]]; then
		shotgun $selection ~/Pictures/screenshots/Screenshot_$(date +"%Y-%m-%d-%H%M%S.png")
	else
		shotgun $selection - | xclip -t 'image/png' -selection clipboard
	fi
elif [[ $1 == "file" ]]; then
	shotgun ~/Pictures/screenshots/Screenshot_$(date +"%Y-%m-%d-%H%M%S.png")
else
	shotgun - | xclip -t 'image/png' -selection clipboard
fi
