#!/bin/bash

echo Starting mariadb
sudo systemctl start mariadb.service

echo Starting apache
sudo systemctl start httpd.service
