#!/bin/bash
if [ $1 ]
then
	if [ $WAYLAND_DISPLAY ]
	then
		swaymsg "output HDMI-A-1 resolution" $1 "position 1920 0"
	elif [ $DISPLAY ]
	then
		xrandr --output HDMI-1 --mode $1
	fi
else
	echo "Specify resolution"
fi
