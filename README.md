# My dotfiles
This is all of the files that I use to configure my linux computer.

There are two scripts in the root of the repository: `unload.sh` and `update.sh`.

These scripts are used to sync the changes in the repository with the file system.
unload will copy all the files under `./files` to your home.

and update will read `./tracked.txt` and copy all of the tracked files
to `./files`.

the `./files/cli` folder contans a bunch of diffrent scripts to be used for a
lot of diffrent things. some are hardcoded to my home directory so if you want
to use them be shure to check.
